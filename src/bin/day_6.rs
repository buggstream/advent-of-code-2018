use advent_of_code_2018::read_puzzle_input;
use std::collections::HashMap;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(6)?;
    let coords = parse_coords(&input);
    let max_area = puzzle_one(&coords);

    println!("{:?}", max_area);

    let safe_area = puzzle_two(&coords, 10000);

    println!("{:?}", safe_area);

    Ok(())
}

fn puzzle_one(coords: &[(i64, i64)]) -> Option<usize> {
    let min_x = coords.iter().map(|(x, _)| *x).min().unwrap();
    let max_x = coords.iter().map(|(x, _)| *x).max().unwrap();
    let min_y = coords.iter().map(|(_, y)| *y).min().unwrap();
    let max_y = coords.iter().map(|(_, y)| *y).max().unwrap();

    let mut coord_areas = HashMap::new();

    for board_y in min_y..=max_y {
        for board_x in min_x..=max_x {
            let closest = coords.iter().copied().min_by_key(|(coord_x, coord_y)| {
                (board_x - coord_x).abs() + (board_y - coord_y).abs()
            });

            if let Some(closest) = closest {
                let area_size = coord_areas.entry(closest).or_insert(Some(0));

                if board_x == min_x || board_x == max_x || board_y == min_y || board_y == max_y {
                    *area_size = None;
                } else if let Some(count) = area_size {
                    *count += 1_usize;
                }
            }
        }
    }

    coord_areas
        .values()
        .filter_map(|area_size| *area_size)
        .max()
}

fn puzzle_two(coords: &[(i64, i64)], distance_max: i64) -> usize {
    let min_x = coords.iter().map(|(x, _)| *x).min().unwrap();
    let max_x = coords.iter().map(|(x, _)| *x).max().unwrap();
    let min_y = coords.iter().map(|(_, y)| *y).min().unwrap();
    let max_y = coords.iter().map(|(_, y)| *y).max().unwrap();
    let mut region_size = 0;

    for board_y in min_y..=max_y {
        for board_x in min_x..=max_x {
            let distance: i64 = coords
                .iter()
                .copied()
                .map(|(coord_x, coord_y)| (board_x - coord_x).abs() + (board_y - coord_y).abs())
                .sum();

            if distance < distance_max {
                region_size += 1;
            }
        }
    }

    region_size
}

fn parse_coords(input: &str) -> Vec<(i64, i64)> {
    input
        .lines()
        .map(|line| {
            let index = line.find(',').unwrap();
            (
                line[..index].trim().parse().unwrap(),
                line[index + 1..].trim().parse().unwrap(),
            )
        })
        .collect()
}

#[cfg(test)]
mod test {
    use crate::{parse_coords, puzzle_one, puzzle_two};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/examples/day_6_example")?;
        let coords = parse_coords(&input);
        let max_area = puzzle_one(&coords);

        assert_eq!(Some(17), max_area);

        Ok(())
    }

    #[test]
    fn puzzle_two_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/examples/day_6_example")?;
        let coords = parse_coords(&input);
        let max_area = puzzle_two(&coords, 32);

        assert_eq!(16, max_area);

        Ok(())
    }
}
