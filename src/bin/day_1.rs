use advent_of_code_2018::read_puzzle_input;
use std::collections::HashSet;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(1)?;
    let frequency_changes: Vec<_> = input
        .lines()
        .map(|line| line.parse::<i32>().unwrap())
        .collect();
    let sum = puzzle_one(&frequency_changes);
    println!("Resulting frequency: {}", sum);

    let duplicate_frequency = puzzle_two(&frequency_changes);
    println!("Duplicate frequency: {:?}", duplicate_frequency);

    Ok(())
}

fn puzzle_one(frequency_updates: &[i32]) -> i32 {
    frequency_updates.iter().sum()
}

fn puzzle_two(frequency_updates: &[i32]) -> i32 {
    let mut reached_frequencies = HashSet::new();
    let mut current_frequency = 0;

    loop {
        for update in frequency_updates {
            current_frequency += update;
            let inserted = reached_frequencies.insert(current_frequency);

            if !inserted {
                return current_frequency;
            }
        }
    }
}
