use advent_of_code_2018::read_puzzle_input;
use std::cmp::min;
use std::error::Error;
use std::iter::FromIterator;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(5)?;
    let polymer = Vec::from_iter(input.trim().chars());
    let units_left = puzzle_one(polymer.clone());
    println!("Units left: {:?}", units_left);

    let least_units_left = puzzle_two(polymer.clone());
    println!("Least units left: {:?}", least_units_left);

    Ok(())
}

fn puzzle_one(mut polymer: Vec<char>) -> Option<u64> {
    if polymer.len() < 2 {
        return None;
    }
    let mut index = polymer.len() - 2;

    while let (Some(&left_unit), Some(&right_unit)) = (polymer.get(index), polymer.get(index + 1)) {
        let left_unit_opposite = match left_unit.is_ascii_lowercase() {
            true => left_unit.to_ascii_uppercase(),
            false => left_unit.to_ascii_lowercase(),
        };

        if left_unit_opposite == right_unit {
            polymer.remove(index + 1);
            polymer.remove(index);
        }
        if index == 0 {
            break;
        }
        index -= 1;
    }

    Some(polymer.len() as u64)
}

fn puzzle_two(polymer: Vec<char>) -> Option<u64> {
    let mut least_units_left = None;
    for letter in 'a'..='z' {
        let simplified_chain: Vec<_> = polymer
            .iter()
            .cloned()
            .filter(|unit| unit.to_ascii_lowercase() != letter)
            .collect();
        let current_units_left = match puzzle_one(simplified_chain) {
            Some(current_units_left) => current_units_left,
            None => continue,
        };
        least_units_left = Some(min(
            least_units_left.unwrap_or(current_units_left),
            current_units_left,
        ));
    }

    least_units_left
}

#[cfg(test)]
mod test {
    use crate::{puzzle_one, puzzle_two};
    use std::error::Error;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = "dabAcCaCBAcCcaDA";
        let units_left = puzzle_one(input.chars().collect());

        assert_eq!(Some(10), units_left);

        Ok(())
    }

    #[test]
    fn puzzle_two_example() -> Result<(), Box<dyn Error>> {
        let input = "dabAcCaCBAcCcaDA";
        let units_left = puzzle_two(input.chars().collect());

        assert_eq!(Some(4), units_left);

        Ok(())
    }
}
