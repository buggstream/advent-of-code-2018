use advent_of_code_2018::read_puzzle_input;
use std::collections::HashMap;
use std::convert::TryInto;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(2)?;
    let ids: Vec<_> = input.lines().collect();
    let checksum = puzzle_one(&ids);

    println!("Box ids checksum: {}", checksum);

    let same_letters = puzzle_two(&ids);

    println!("Same letters: {:?}", same_letters);

    Ok(())
}

fn puzzle_one(ids: &[&str]) -> u32 {
    let mut twos = 0;
    let mut threes = 0;

    for id in ids {
        let mut letter_count = HashMap::new();
        for letter in id.chars() {
            let count = letter_count.entry(letter).or_insert(0_u32);
            *count += 1;
        }

        if count_letter_occurrences(&letter_count, 2) > 0 {
            twos += 1;
        }

        if count_letter_occurrences(&letter_count, 3) > 0 {
            threes += 1;
        }
    }

    twos * threes
}

fn count_letter_occurrences(letter_count: &HashMap<char, u32>, required_count: u32) -> u32 {
    letter_count
        .iter()
        .filter(|(_, count)| **count == required_count)
        .count()
        .try_into()
        .unwrap()
}

fn puzzle_two(ids: &[&str]) -> Option<String> {
    for (index, id) in ids.iter().enumerate() {
        for id_cmp in ids.iter().skip(index) {
            let (same_indices, different_indices) =
                id.chars().zip(id_cmp.chars()).enumerate().fold(
                    (Vec::new(), Vec::new()),
                    |(mut same, mut different), (letter_index, (left, right))| {
                        if left == right {
                            same.push(letter_index);
                        } else {
                            different.push(letter_index);
                        }

                        (same, different)
                    },
                );

            if different_indices.len() == 1 {
                let mut same_letters = String::new();
                for letter_index in same_indices {
                    same_letters.push(id.chars().nth(letter_index).unwrap());
                }

                return Some(same_letters);
            }
        }
    }

    None
}
