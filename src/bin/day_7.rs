use advent_of_code_2018::read_puzzle_input;
use lazy_static::lazy_static;
use petgraph::graph::DiGraph;
use petgraph::prelude::EdgeRef;
use petgraph::Direction;
use regex::Regex;
use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::error::Error;
use petgraph::dot::Dot;

lazy_static! {
    static ref DEPENDENCY: Regex = Regex::new(
        r"(?m)^Step ([[:alpha:]]+) must be finished before step ([[:alpha:]]+) can begin.$"
    )
    .unwrap();
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(7)?;
    let steps = parse_steps(&input);
    let order_str = puzzle_one(&steps);

    println!("{}", order_str);

    Ok(())
}

fn puzzle_one(steps: &[(&str, &str)]) -> String {
    let mut graph = DiGraph::new();
    let mut nodes = HashMap::new();

    for (before, after) in steps {
        let before_node = *nodes
            .entry(*before)
            .or_insert_with(|| graph.add_node(*before));
        let after_node = *nodes
            .entry(*after)
            .or_insert_with(|| graph.add_node(*after));
        graph.add_edge(before_node, after_node, ());
    }

    println!("{:?}", Dot::new(&graph));

    let mut order = String::new();
    let mut visited = HashSet::new();
    let mut visitable = BinaryHeap::new();
    let starting_nodes = graph
        .node_indices()
        .filter(|node| graph.edges_directed(*node, Direction::Incoming).count() == 0)
        .map(|node| Reverse(*graph.node_weight(node).unwrap()));

    visitable.extend(starting_nodes);

    while let Some(Reverse(node)) = visitable.pop() {
        if visited.contains(&node) {
            continue;
        }

        println!("visiting node: {:?}", node);

        visited.insert(node);
        order.push_str(node);

        for outgoing_edge in graph.edges_directed(nodes[node], Direction::Outgoing) {
            let (_, end_node) = graph.edge_endpoints(outgoing_edge.id()).unwrap();

            let end_node_name = *graph.node_weight(end_node).unwrap();
            println!("  end node: {:?}", end_node_name);

            let can_be_visited =
                graph
                    .edges_directed(end_node, Direction::Incoming)
                    .all(|incoming_edge| {
                        let (start_node, _) = graph.edge_endpoints(incoming_edge.id()).unwrap();
                        visited.contains(graph.node_weight(start_node).unwrap())
                    });

            if can_be_visited {
                println!("  visitable: {}", end_node_name);
                visitable.push(Reverse(graph.node_weight(end_node).unwrap()));
            }
        }
    }

    order
}

fn parse_steps(input: &str) -> Vec<(&str, &str)> {
    DEPENDENCY
        .captures_iter(input)
        .map(|captures| {
            (
                captures.get(1).unwrap().as_str(),
                captures.get(2).unwrap().as_str(),
            )
        })
        .collect()
}

#[cfg(test)]
mod test {
    use crate::{parse_steps, puzzle_one};
    use std::error::Error;
    use std::fs::read_to_string;

    #[test]
    fn puzzle_one_example() -> Result<(), Box<dyn Error>> {
        let input = read_to_string("inputs/examples/day_7_simple")?;
        let steps = parse_steps(&input);
        let order = puzzle_one(&steps);

        assert_eq!("CABDFE", order.as_str());

        Ok(())
    }
}
