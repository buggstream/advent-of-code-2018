use advent_of_code_2018::read_puzzle_input;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(3)?;
    let claims = parse_claims(&input);

    let overlapping_space = puzzle_one(&claims);
    println!("Overlapping area: {}", overlapping_space);

    let non_overlapping_id = puzzle_two(&claims);
    println!("{:?}", non_overlapping_id);

    Ok(())
}

fn puzzle_one(claims: &[Claim]) -> usize {
    let mut used_space = HashMap::new();

    for claim in claims {
        for x in (claim.offset_x)..(claim.offset_x + claim.width) {
            for y in (claim.offset_y)..(claim.offset_y + claim.height) {
                let count = used_space.entry((x, y)).or_insert(0_u32);
                *count += 1;
            }
        }
    }

    used_space.values().filter(|count| **count > 1).count()
}

fn puzzle_two(claims: &[Claim]) -> Option<u32> {
    let mut used_space = HashMap::new();

    for claim in claims {
        for x in (claim.offset_x)..(claim.offset_x + claim.width) {
            for y in (claim.offset_y)..(claim.offset_y + claim.height) {
                let claimed_by = used_space.entry((x, y)).or_insert(Vec::new());
                claimed_by.push(claim.id);
            }
        }
    }

    let all_ids: HashSet<_> = claims.iter().map(|claim| claim.id).collect();
    let overlapping_ids: HashSet<_> = used_space
        .values()
        .cloned()
        .filter(|claimed_by| claimed_by.len() > 1)
        .flatten()
        .collect();

    let non_overlapping_ids: Vec<_> = all_ids.difference(&overlapping_ids).collect();

    if non_overlapping_ids.len() == 1 {
        return Some(*non_overlapping_ids[0]);
    }

    None
}

#[derive(Debug)]
struct Claim {
    id: u32,
    offset_x: u32,
    offset_y: u32,
    width: u32,
    height: u32,
}

fn parse_claims(input: &str) -> Vec<Claim> {
    let mut claims = Vec::new();

    let regex = Regex::new(r"(?m)^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$").unwrap();

    for captures in regex.captures_iter(input) {
        let parsed: Result<Vec<_>, _> = captures
            .iter()
            .skip(1)
            .map(|capture_match| capture_match.unwrap().as_str().parse::<u32>())
            .collect();

        let values = parsed.unwrap();

        let claim = Claim {
            id: values[0],
            offset_x: values[1],
            offset_y: values[2],
            width: values[3],
            height: values[4],
        };

        claims.push(claim);
    }

    claims
}
