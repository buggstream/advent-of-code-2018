use advent_of_code_2018::read_puzzle_input;
use chrono::prelude::*;
use chrono::{Duration, ParseError};
use parse_display::FromStr as PFromStr;
use std::collections::HashMap;
use std::error::Error;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_puzzle_input(4)?;
    let events = parse_events(&input)?;
    let sleep_events = sleep_events(&events);

    let sleepiest_guard = puzzle_one(&sleep_events);
    println!("{:?}", sleepiest_guard);

    let sleepiest_at_minute = puzzle_two(&sleep_events);
    println!("{:?}", sleepiest_at_minute);

    Ok(())
}

fn puzzle_one(events: &[(u32, DateTime<Utc>, DateTime<Utc>)]) -> Option<u32> {
    let mut sleep_tracker = HashMap::new();

    for (guard, start_sleep, end_sleep) in events {
        let (sleep_duration, minutes_asleep) = sleep_tracker
            .entry(*guard)
            .or_insert((Duration::minutes(0), [0; 60]));
        *sleep_duration = *sleep_duration + (*end_sleep - *start_sleep);

        for minute in start_sleep.minute()..end_sleep.minute() {
            minutes_asleep[minute as usize] += 1;
        }
    }

    let (sleepiest_guard, (_, minutes_asleep)) = sleep_tracker
        .iter()
        .max_by_key(|(_, (duration, _))| duration.num_minutes())
        .unwrap();

    let (most_asleep_minute, _) = minutes_asleep
        .iter()
        .enumerate()
        .max_by_key(|(_, amount)| **amount)
        .unwrap();

    return Some(sleepiest_guard * most_asleep_minute as u32);
}

fn puzzle_two(events: &[(u32, DateTime<Utc>, DateTime<Utc>)]) -> Option<u32> {
    let mut sleep_tracker = HashMap::new();

    for (guard, start_sleep, end_sleep) in events {
        for minute in start_sleep.minute()..end_sleep.minute() {
            let times_asleep = sleep_tracker.entry((minute, *guard)).or_insert(0_u32);
            *times_asleep += 1;
        }
    }

    sleep_tracker
        .into_iter()
        .max_by_key(|(_, times_asleep)| *times_asleep)
        .map(|((minute, guard), _)| guard * minute)
}

fn sleep_events(events: &[Event]) -> Vec<(u32, DateTime<Utc>, DateTime<Utc>)> {
    let mut events: Vec<_> = events.iter().collect();
    events.sort_by_key(|event| event.time.0);

    let mut guard_on_shift = None;
    let mut sleeping_since = None;

    events.iter().fold(Vec::new(), |mut acc, event| {
        match event.event_type {
            EventType::BeginShift(guard) => guard_on_shift = Some(guard),
            EventType::FallAsleep => sleeping_since = Some(event.time.0),
            EventType::WakeUp => acc.push((
                guard_on_shift.unwrap(),
                sleeping_since.unwrap(),
                event.time.0,
            )),
        }

        acc
    })
}

fn parse_events(input: &str) -> Result<Vec<Event>, impl Error> {
    input.lines().map(|line| line.parse::<Event>()).collect()
}

#[derive(Debug, Clone)]
struct DateTimeUtcWrapper(DateTime<Utc>);

impl FromStr for DateTimeUtcWrapper {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parsed = Utc.datetime_from_str(s, "%Y-%m-%d %H:%M")?;
        Ok(DateTimeUtcWrapper(parsed))
    }
}

#[derive(PFromStr, Debug, Clone)]
#[display("[{time}] {event_type}")]
struct Event {
    time: DateTimeUtcWrapper,
    event_type: EventType,
}

#[derive(PFromStr, Debug, Clone)]
enum EventType {
    #[display("Guard #{0} begins shift")]
    BeginShift(u32),
    #[display("falls asleep")]
    FallAsleep,
    #[display("wakes up")]
    WakeUp,
}
