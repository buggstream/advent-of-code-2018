use std::error::Error;
use std::fs::File;
use std::io::Read;

pub fn read_puzzle_input(day: u32) -> Result<String, Box<dyn Error>> {
    let file_name = format!("inputs/day_{}", day);
    let mut file_contents = String::new();
    File::open(&file_name)?.read_to_string(&mut file_contents)?;
    Ok(file_contents)
}
